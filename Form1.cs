﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HW19
{
    public partial class Form1 : Form
    {
        Dictionary<string, string> users = new Dictionary<string, string>();//to save the users
        public Form1()
        {
            InitializeComponent();
            password_txt.PasswordChar = '*';
            password_txt.MaxLength = 10;
            getUsers();//init users
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || password_txt.Text == "")
            {
                label3.Text = "אחד מהשדות רקים";
            }
            else if(users.ContainsKey(textBox1.Text))
            {
                if(users[textBox1.Text]== password_txt.Text)
                {
                    Form2 wnd = new Form2();
                    wnd.UserNameIn = UserNameBox;
                    this.Hide();
                    wnd.ShowDialog();
                    this.Close();
                }
                else
                {
                    label3.Text = "ססמא שגויה";
                }
            }
            else
            {
                label3.Text = "שם משתמש לא קיים";
            }
        }

        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {
            
        }

        private void button1_MouseClick(object sender, MouseEventArgs e)
        {

        }
        public string UserNameBox
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        public void getUsers()
        {
            string[] lines = System.IO.File.ReadAllLines("Users.txt");//reading the lines from the file
            for (int i = 0; i < lines.Length; i++)
            {
                string[] sub = lines[i].Split(',');//spliting to username anpassword
                users.Add(sub[0], sub[1]);//adding the new user
            }
        }
    }
}
