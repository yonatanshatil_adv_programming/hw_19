﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HW19
{
    public partial class Form2 : Form
    {
        string userName;
        Dictionary<string, string> birthdays = new Dictionary<string, string>();//to save the users
        public Form2()
        {
            InitializeComponent();
            label2.Text = "";
        }
        
        public string UserNameIn
        {
            get { return userName; }
            set { userName = value; }
        }
        public void loadBD()
        {
            string FileName = userName + "BD.txt";
            string[] lines = System.IO.File.ReadAllLines(FileName);//reading the lines from the file
            for (int i = 0; i < lines.Length; i++)
            {
                string[] sub = lines[i].Split(',');//spliting to date and name
                birthdays.Add(sub[1], sub[0]);
            }
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            if(birthdays.Count==0)
            {
                loadBD();
            }
            string dateString = e.Start.Month.ToString() + "/" + e.Start.Day.ToString() + "/" + e.Start.Year.ToString();
            if(birthdays.ContainsKey(dateString))
            {
                label2.Text = "בתאריך הנבחר-" + birthdays[dateString] + " חוגג יום הולדת!";
            }
            else
            {
                label2.Text = "בתאריך הנבחר-" + "אף אחד לא" + " חוגג יום הולדת";
            }
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
